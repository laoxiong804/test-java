package net.reyhung;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!");
    }

    public static void test(final String[] args) {
        System.out.println("test");
    }
}


class Test extends Thread {
    /*
     * @Auth: ReyHung
    */
    public Test(String arg) {
        /*
         * @Auth: ReyHung
        */
        System.out.println(arg);
    }
}